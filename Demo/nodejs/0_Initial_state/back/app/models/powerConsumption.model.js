const mongoose = require('mongoose');

const MetadataSchema = mongoose.Schema({
  socket: Number,
  event: String
});

const PowerConsumptionSchema = mongoose.Schema({
    timestamp: Date,
    sensor: String,
    target: String,
    power: Number,
    metadata: MetadataSchema
}, {
    timestamps: true,
    collection : 'power_consumption'
});

module.exports = mongoose.model('PowerConsumption', PowerConsumptionSchema);
