const PowerConsumption = require('../models/powerConsumption.model.js');


exports.findAll = (req, res) => {
  PowerConsumption.find({
    "metadata.event": 'RAPL_ENERGY_GPU'
  }).sort({timestamp: -1}).limit(1000)
      .then(sensors => {
          res.send(sensors);
      }).catch(err => {
          res.status(500).send({
              message: err.message || "Some error occurred while retrieving sensors."
          });
      });
};


exports.findOne = (req, res) => {
  PowerConsumption.findById(req.params.dataId)
      .then(sensor => {
          if(!sensor) {
              return res.status(404).send({
                  message: "Sensor not found with id " + req.params.dataId
              });
          }
          res.send(sensor);
      }).catch(err => {
          if(err.kind === 'ObjectId') {
              return res.status(404).send({
                  message: "Sensor not found with id " + req.params.dataId
              });
          }
          return res.status(500).send({
              message: "Error retrieving Sensor with id " + req.params.dataId
          });
      });
};
