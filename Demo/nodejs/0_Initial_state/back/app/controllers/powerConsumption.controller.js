const PowerConsumption = require('../models/powerConsumption.model.js');


exports.getTotal = (req, res) => {

  PowerConsumption.aggregate( [
    {
        $group : {
            _id : '$sensor',
            totalAmount : {$sum : '$power'}    //sums the amount
        }
    }
  ]).then(function(result) {
    res.send(result);
  });

}

exports.getMaximum = (req, res) => {

  PowerConsumption.findOne().sort({power: -1}).then(function(result) {
    res.send({
      value: result.power,
      timestamp: result.timestamp
    })
  });

}

exports.getLast24 = (req, res) => {
  PowerConsumption.aggregate( [
    {
        $match : {
          'timestamp': { $gt: new Date(Date.now() - 24*60*60 * 1000)}
        }
    },
    {
        $group : {
            _id : '$sensor',
            totalAmount : {$sum : '$power'}    //sums the amount
        }
    }
  ]).then(function(result) {
    res.send(result);
  });
}

exports.create = (req, res) => {
  // Validate request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Sensor content can not be empty"
        });
    }

    // Create a Sensor
    const sensor = new PowerConsumption({
        title: req.body.title || "Untitled Note",
        content: req.body.content
    });

    // Save Sensor in the database
    sensor.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Sensor."
        });
    });
};


exports.findAll = (req, res) => {
  PowerConsumption.find().sort({timestamp: -1})
      .then(sensors => {
          res.send(sensors);
      }).catch(err => {
          res.status(500).send({
              message: err.message || "Some error occurred while retrieving sensors."
          });
      });
};


exports.findOne = (req, res) => {
  PowerConsumption.findById(req.params.dataId)
      .then(sensor => {
          if(!sensor) {
              return res.status(404).send({
                  message: "Sensor not found with id " + req.params.dataId
              });
          }
          res.send(sensor);
      }).catch(err => {
          if(err.kind === 'ObjectId') {
              return res.status(404).send({
                  message: "Sensor not found with id " + req.params.dataId
              });
          }
          return res.status(500).send({
              message: "Error retrieving Sensor with id " + req.params.dataId
          });
      });
};


exports.update = (req, res) => {
  // Validate Request
     if(!req.body.content) {
         return res.status(400).send({
             message: "Sensor content can not be empty"
         });
     }

     // Find Sensor and update it with the request body
     PowerConsumption.findByIdAndUpdate(req.params.dataId, {
         title: req.body.title || "Untitled Note",
         content: req.body.content
     }, {new: true})
     .then(sensor => {
         if(!sensor) {
             return res.status(404).send({
                 message: "Sensor not found with id " + req.params.dataId
             });
         }
         res.send(sensor);
     }).catch(err => {
         if(err.kind === 'ObjectId') {
             return res.status(404).send({
                 message: "Sensor not found with id " + req.params.dataId
             });
         }
         return res.status(500).send({
             message: "Error updating Sensor with id " + req.params.dataId
         });
     });
};


exports.delete = (req, res) => {
  PowerConsumption.findByIdAndRemove(req.params.dataId)
      .then(sensor => {
          if(!sensor) {
              return res.status(404).send({
                  message: "Sensor not found with id " + req.params.dataId
              });
          }
          res.send({message: "Sensor deleted successfully!"});
      }).catch(err => {
          if(err.kind === 'ObjectId' || err.name === 'NotFound') {
              return res.status(404).send({
                  message: "Sensor not found with id " + req.params.dataId
              });
          }
          return res.status(500).send({
              message: "Could not delete Sensor with id " + req.params.dataId
          });
      });
};
