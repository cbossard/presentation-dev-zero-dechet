/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardHeader from "../../components/Card/CardHeader.jsx";
import CardBody from "../../components/Card/CardBody.jsx";

const style = {
  card: {
     maxWidth: 10
   },
   bullet: {
     display: 'inline-block',
     margin: '0 2px',
     transform: 'scale(0.8)',
   },
   title: {
     fontSize: 14,
   },
   pos: {
     marginBottom: 12,
   },
};
function TipsPage(props) {
  const { classes } = props;
  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={classes.cardTitleWhite}>Eco-conception web</h4>
        <p className={classes.cardCategoryWhite}>
          Bonnes pratiques
        </p>
      </CardHeader>
      <CardBody>
        <Card>
          <CardContent>
            <Typography color="textSecondary" gutterBottom>
              Etape : 1. Conception / Catégorie : 1. Fonctionnelle
            </Typography>
            <Typography variant="h5" component="h2">
               Eliminer les fonctionnalités non essentielles
             </Typography>
             <Typography className={classes.pos} color="textSecondary">
               Priorité : 1
             </Typography>
           </CardContent>
           <CardActions>
             <Button size="small">Learn More</Button>
           </CardActions>
        </Card>
        <Card>
          <CardContent>
            <Typography color="textSecondary" gutterBottom>
              Etape : 1. Conception / Catégorie : 1. Fonctionnelle
            </Typography>
            <Typography variant="h5" component="h2">
            Quantifier précisément le besoin
             </Typography>
             <Typography className={classes.pos} color="textSecondary">
               Priorité : 1
             </Typography>
           </CardContent>
           <CardActions>
             <Button size="small">Learn More</Button>
           </CardActions>
         </Card>
         <Card>
           <CardContent>
             <Typography color="textSecondary" gutterBottom>
               Etape : 1. Conception / Catégorie : 1. Fonctionnelle
             </Typography>
             <Typography variant="h5" component="h2">
             Fluidifier le processus
              </Typography>
              <Typography className={classes.pos} color="textSecondary">
                Priorité : 1
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small">Learn More</Button>
            </CardActions>
          </Card>
          <Card>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                Etape : 1. Conception / Catégorie : 1. Fonctionnelle
              </Typography>
              <Typography variant="h5" component="h2">
              Préférer la saisie assistée à l'autocompletion
               </Typography>
               <Typography className={classes.pos} color="textSecondary">
                 Priorité : 1
               </Typography>
             </CardContent>
             <CardActions>
               <Button size="small">Learn More</Button>
             </CardActions>
           </Card>
          </CardBody>
        </Card>
  );
}

TipsPage.propTypes = {
  classes: PropTypes.object
};

export default withStyles(style)(TipsPage);
