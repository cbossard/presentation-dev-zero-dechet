/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// ##############################
// // // javascript library for creating charts
// #############################

// ##############################
// // // variables used to create animation on charts
// #############################


// ##############################
// // // Daily Sales
// #############################

const dailySalesChart = {
  data: {
    labels: ["M", "T", "W", "T", "F", "S", "S"],
    series: [[0, 3.50665283203125, 6.48492431640625, 12.92022705078125, 9.37701416015625, 4.17919921875, 4.06158447265625, 3.25103759765625,3.61480712890625, 5.2672119140625, 9.510009765625, 12.39056396484375, 13.53363037109375,15.98394775390625, 12.16326904296875, 15.4078369140625, 14.84002685546875, 15.2296142578125, 14.8929443359375,14.13128662109375, 14.7557373046875, 13.58062744140625,15.3834228515625,15.09912109375, 15.02923583984375,  14.75732421875, 14.6802978515625   ]]
  },
  options: {
    axisX: {
      showGrid: false
    },
    low: 0,
  //  high: 1000,
    chartPadding: {
      top: 0,
      right: 5,
      bottom: 0,
      left: 0
    }
  }
};


module.exports = {
  dailySalesChart
};
