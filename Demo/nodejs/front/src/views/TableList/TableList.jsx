/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
// core components
import GridItem from "../../components/Grid/GridItem.jsx";
import GridContainer from "../../components/Grid/GridContainer.jsx";
import Card from "../../components/Card/Card.jsx";
import CardHeader from "../../components/Card/CardHeader.jsx";
import CardBody from "../../components/Card/CardBody.jsx";

import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components



const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

class TableList extends React.Component {
  constructor(props){
        super(props);
        this.state = {
          pkgEnergy:[],
          gpuEnergy: [],
          coresEnergy: []
        }
    }

    componentWillMount(){
        this.getChartData();
        console.log(this.state)
    }

    getChartData(){

      fetch('http://localhost:3001/pkgConsumptions')
          .then(Response =>  Response.json())
          .then(res => {
             this.setState({
                pkgEnergy:res
              });

          })
          .catch(error => {
              console.log(error)
          });

      fetch('http://localhost:3001/gpuConsumptions')
        .then(Response =>  Response.json())
        .then(res => {
           this.setState({
              gpuEnergy:res
            });

        })
        .catch(error => {
            console.log(error)
        });

      fetch('http://localhost:3001/coresConsumptions')
        .then(Response =>  Response.json())
        .then(res => {
           this.setState({
              coresEnergy:res
            });

        })
        .catch(error => {
            console.log(error)
        });
    }

    render() {

console.log("render")
console.log(this.state)

      const { classes } = this.props;

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>RAPL_ENERGY_PKG Data</h4>
            <p className={classes.cardCategoryWhite}>
              Last 1000 entries
            </p>
          </CardHeader>
          <CardBody>

          <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Timestamp</TableCell>
                    <TableCell align="right">Power</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.pkgEnergy.map(row => (
                    <TableRow key={row._id}>
                      <TableCell>
                        {row.timestamp}
                      </TableCell>
                      <TableCell align="right">{row.power}</TableCell>
                    </TableRow>)
                  )}
                </TableBody>
              </Table>
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>
              RAPL_ENERGY_GPU Data
            </h4>
            <p className={classes.cardCategoryWhite}>
              Here is a subtitle for this table
            </p>
          </CardHeader>
          <CardBody>
            <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Timestamp</TableCell>
                    <TableCell align="right">Power</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.gpuEnergy.map(row => (
                    <TableRow key={row.name}>
                      <TableCell component="th" scope="row">
                        {row.timestamp}
                      </TableCell>
                      <TableCell align="right">{row.power}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>
              RAPL_ENERGY_CORES Data
            </h4>
            <p className={classes.cardCategoryWhite}>
              Here is a subtitle for this table
            </p>
          </CardHeader>
          <CardBody>
            <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Timestamp</TableCell>
                    <TableCell align="right">Power</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.coresEnergy.map(row => (
                    <TableRow key={row.name}>
                      <TableCell component="th" scope="row">
                        {row.timestamp}
                      </TableCell>
                      <TableCell align="right">{row.power}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}
}

TableList.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(TableList);
