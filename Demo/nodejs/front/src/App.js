import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

// core components
import Admin from "./layouts/Admin.jsx";

class App extends React.Component {
  render () {
    const hist = createBrowserHistory();

    return (
      ReactDOM.render(
          <Router history={hist}>
            <Switch>
              <Route
                path="/admin"
                render={(props) => <Admin {...props} />} />
              <Redirect from="/" to="/admin/dashboard" />
            </Switch>
          </Router>,
        document.getElementById("root")
      )
    );
  }
}

export default App;
