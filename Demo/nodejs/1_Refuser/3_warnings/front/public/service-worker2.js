self.addEventListener('fetch', event => {
   
console.log("FETCH");

  const fetchResponse = fetch(event.request);

  event.respondWith(fetchResponse.then(response => {
    // Get the client.
    self.clients.get(event.clientId).then(function(client) {
      // Exit early if we don't get the client.
      // Eg, if it closed.
      if (!client) return;

      // Send a message to the client.
      client.postMessage({
        msg: "Hey I just got a fetch from you !",
        url: event.request.url,
        size: response.headers.get("content-length"),
        content: response.data
      });
    });

    return response;

  }));

});
