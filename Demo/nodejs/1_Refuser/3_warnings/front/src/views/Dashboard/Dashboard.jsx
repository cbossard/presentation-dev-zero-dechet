/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import AccessTime from "@material-ui/icons/AccessTime";
// core components
import GridItem from "../../components/Grid/GridItem.jsx";
import GridContainer from "../../components/Grid/GridContainer.jsx";
import Card from "../../components/Card/Card.jsx";
import CardHeader from "../../components/Card/CardHeader.jsx";
import CardIcon from "../../components/Card/CardIcon.jsx";
import CardBody from "../../components/Card/CardBody.jsx";
import CardFooter from "../../components/Card/CardFooter.jsx";

import {computeEcoIndex} from "../../variables/ecoIndex.jsx"



import {
  dailySalesChart
} from "../../variables/charts.jsx";

import dashboardStyle from "../../assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

var nbRequestPage = 0;
var receivedDataSize = 0;

navigator.serviceWorker.addEventListener('message', function(event) {

  nbRequestPage++;
  if(event.data.size) {
    receivedDataSize += Number(event.data.size);
  }

});


function addData(data) {
  var resultArray = [];
  for (var i = 0; i < data.length; i++) {
        resultArray.push(data[i].power);
  }

  return resultArray;
}


class Dashboard extends React.Component {

  constructor(props){

        super(props);

        this.state = {
          ecoIndex: {},
          pkgEnergy:{},
          gpuEnergy: {},
          coresEnergy: {}
        }
    }

    componentWillMount(){
      this.fetchData();
      this.timer = setInterval(()=> this.fetchData(), 10000)
    }


    fetchData() {
      fetch('http://localhost:3001/pkgConsumptions')
          .then(Response =>  Response.json())
          .then(res => {
            console.log("pouet")
             var result = addData(res);
             this.setState({
                pkgEnergy:{
                  series: [
                        result
                      ]
                  }
              });

          })
          .catch(error => {
              console.log(error)
          });

      fetch('http://localhost:3001/gpuConsumptions')
          .then(Response =>  Response.json())
          .then(res => {
             var result = addData(res);
             this.setState({
                gpuEnergy:{
                  series: [
                        result
                      ]
                  }
              });

          })
          .catch(error => {
              console.log(error)
          });

      fetch('http://localhost:3001/coresConsumptions')
          .then(Response =>  Response.json())
          .then(res => {
             var result = addData(res);
             this.setState({
                coresEnergy:{
                  series: [
                        result
                      ]
                  }
              });

          })
          .catch(error => {
              console.log(error)
          });


    }


    render() {
      const { classes } = this.props;

      this.state.ecoIndex = computeEcoIndex(nbRequestPage, receivedDataSize);

      return (
        <div>
          <GridContainer>

              <GridItem xs={12} sm={6} md={3}>
                <Card>
                  <CardHeader color="info" stats icon>
                    <CardIcon color="info">
                      <Icon>wb_sunny</Icon>
                    </CardIcon>
                    <p className={classes.cardCategory}>Eco-Index</p>
                    <h3 className={classes.cardTitle}>{this.state.ecoIndex.grade}</h3>
                  </CardHeader>
                  <CardFooter stats>
                    <div className={classes.stats}>
                      Nombre de requêtes : {this.state.ecoIndex.nbRequest} <br/>
                      Taille de la page (kb) : {this.state.ecoIndex.pageSize} <br/>
                      Taille du DOM : {this.state.ecoIndex.domSize} <br/>
                      GES (gCO2e) : {this.state.ecoIndex.GES} <br/>
                      Eau (cl) : {this.state.ecoIndex.water} <br/>
                      EcoIndex : {this.state.ecoIndex.ecoIndex} <br/>
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
            </GridContainer>
            <GridContainer>
            <GridItem xs={12} sm={12} md={4}>
              <Card chart>
                <CardHeader color="success">

                <ChartistGraph
                  className="ct-chart"
                  data={this.state.pkgEnergy}
                  type="Line"
                  options={dailySalesChart.options}
                  responsiveOptions={dailySalesChart.responsiveOptions}
                  listener={dailySalesChart.animation}
                />

                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Running Average Power Limit Package (RAPL_ENERGY_PKG)</h4>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <AccessTime /> updated 4 minutes ago
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <Card chart>
                <CardHeader color="warning">
                  <ChartistGraph
                    className="ct-chart"
                    data={this.state.gpuEnergy}
                    type="Line"
                    options={dailySalesChart.options}
                    responsiveOptions={dailySalesChart.responsiveOptions}
                    listener={dailySalesChart.animation}
                  />
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Running Average Power Limit GPU (RAPL_ENERGY_GPU)</h4>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <AccessTime /> updated 4 minutes ago
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <Card chart>
                <CardHeader color="danger">
                  <ChartistGraph
                    className="ct-chart"
                      data={this.state.coresEnergy}
                    type="Line"
                    options={dailySalesChart.options}
                    listener={dailySalesChart.animation}
                  />
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Running Average Power Limit Cores (RAPL_ENERGY_CORES)</h4>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <AccessTime /> updated 4 minutes ago
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
          </GridContainer>

        </div>
      );
    }
  }

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
