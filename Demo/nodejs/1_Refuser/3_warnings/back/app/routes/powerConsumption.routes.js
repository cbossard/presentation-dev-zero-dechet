module.exports = (app) => {
    const powerConsumptions = require('../controllers/powerConsumption.controller.js');

    const coresConsumptions = require('../controllers/coresConsumptions.controller.js');

    const gpuConsumptions = require('../controllers/gpuConsumptions.controller.js');

    const pkgConsumptions = require('../controllers/pkgConsumptions.controller.js');

    const userProfile = require('../controllers/userProfile.controller.js');

    //
    // app.post('/powerConsumptions', powerConsumptions.create);
    //
    // app.get('/powerConsumptions', powerConsumptions.findAll);

    //app.get('/powerConsumptions/:dataId', powerConsumptions.findOne);

    //app.put('/powerConsumptions/:dataId', powerConsumptions.update);

    //app.delete('/powerConsumptions/:dataId', powerConsumptions.delete);



    app.get('/coresConsumptions', coresConsumptions.findAll);

    app.get('/coresConsumptions/:dataId', coresConsumptions.findOne);

    app.get('/gpuConsumptions', gpuConsumptions.findAll);

    app.get('/gpuConsumptions/:dataId', gpuConsumptions.findOne);

    app.get('/pkgConsumptions', pkgConsumptions.findAll);

    app.get('/pkgConsumptions/:dataId', pkgConsumptions.findOne);

    app.get('/user/:userId', userProfile.findOne);

    app.get('/users', userProfile.findAll);

    app.get('/powerConsumptions/total', powerConsumptions.getTotal);

    app.get('/powerConsumptions/last24', powerConsumptions.getLast24);

    app.get('/powerConsumptions/maximum', powerConsumptions.getMaximum);

    //app.get('/powerConsumptions/dashboard', powerConsumptions.getDashboard);

}
