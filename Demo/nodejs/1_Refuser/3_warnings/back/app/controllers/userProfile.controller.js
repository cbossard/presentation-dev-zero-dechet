const UserProfile = require('../models/userProfile.model.js');


exports.findOne = (req, res) => {

  UserProfile.findById(req.params.userId)
      .then(user => {
          if(!user) {
              return res.status(404).send({
                  message: "User not found with id " + req.params.userId
              });
          }
          res.send(user);
      }).catch(err => {
          if(err.kind === 'ObjectId') {
              return res.status(404).send({
                  message: "User not found with id " + req.params.userId
              });
          }
          return res.status(500).send({
              message: "Error retrieving User with id " + req.params.userId
          });
      });
};

exports.findAll = (req, res) => {

  UserProfile.find()
      .then(users => {
          res.send(users);
      }).catch(err => {
          if(err.kind === 'ObjectId') {
              return res.status(404).send({
                  message: "User not found with id " + req.params.userId
              });
          }
          return res.status(500).send({
              message: "Error retrieving User with id " + req.params.userId
          });
      });
};

exports.create = (req, res) => {
  // Validate request
    if(!req.body.content) {
        return res.status(400).send({
            message: "User content can not be empty"
        });
    }

    // Create a Sensor
    const user = new UserProfile({
      company: req.body.company,
      username: req.body.username,
      emailAddress: req.body.emailAddress,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      city: req.body.city,
      country: req.body.country,
      postalCode: req.body.postalCode,
      about: req.body.about
    });

    // Save Sensor in the database
    user.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the User."
        });
    });
};
