const mongoose = require('mongoose');


const UserProfileSchema = mongoose.Schema({
    company: String,
    username: String,
    emailAddress: String,
    firstName: String,
    lastName: String,
    city: String,
    country: String,
    postalCode: String,
    about: String
}, {
    timestamps: true,
    collection : 'userProfile'
});

module.exports = mongoose.model('UserProfile', UserProfileSchema);
