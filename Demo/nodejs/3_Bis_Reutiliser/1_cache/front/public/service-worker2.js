var nb_request = 0;
var requestSize = 0;
var CURRENT_CACHE = "FRONT_CACHE";

self.addEventListener('install', (event) => {
  nb_request = 0
  requestSize= 0;
});

// On vide le cache
self.addEventListener('activate', function(event) {
  console.log("ACTIVATE")
    // Active worker won't be treated as activated until promise
    // resolves successfully.
    event.waitUntil(
      caches.keys().then(function(cacheNames) {
        return Promise.all(
          cacheNames.map(function(cacheName) {
            if (cacheName == CURRENT_CACHE) {
              console.log('Deleting out of date cache: ', cacheName);

              return caches.delete(cacheName);
            }
          })
        );
      })
    );
});


self.addEventListener('fetch', event => {
  nb_request++

  const requestUrl = new URL(
   event.request.url
 );

console.log("service worker")

  // On ne met pas en cache les appels vers le back-end
  if(requestUrl.host == "localhost:3001") {
    event.respondWith(fetch(event.request));
  }else {
    event.respondWith(

      caches.open(CURRENT_CACHE).then(function(cache) {
        return cache.match(event.request).then(function(response) {
          if (response) {
              console.log('Found response in cache :', event.request.url);

              return response;
          }
          console.log('Fetching request from the network ' + event.request.url);
          return fetch(event.request).then(function(networkResponse) {

              // Get the client.
              self.clients.get(event.clientId).then(function(client) {
                // Exit early if we don't get the client.
                // Eg, if it closed.
                if (!client) return;

                // Send a message to the client.
                client.postMessage({
                  msg: "Hey I just got a fetch from you ! ",
                  url: event.request.url,
                  size: networkResponse.headers.get("content-length"),
                  content: networkResponse.data
                });
              });

              cache.put(event.request, networkResponse.clone());

              return networkResponse;
          });
        }).catch(function(error) {

          // Handles exceptions that arise from match() or fetch().
          console.error('Error in fetch handler:', error);

          throw error;
        });
      })
    );
  }


});
